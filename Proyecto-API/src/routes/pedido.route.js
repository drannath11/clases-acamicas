const express = require('express');
const router = express.Router();
const {obtenerUsuarios} = require('../models/usuario.model');
const {obtenerProducto} = require('../models/producto.model');
const {obtenerPedidosUsuario} = require('../models/pedido.model')
const {verificationAdmin} = require('../middlewares/Autenticacion.middleware')


router.get('/:username/:password', (req,res) => {
    const {username, password } = req.params; 
    const user = obtenerUsuarios().find(u => u.username === username && u.password === password);
    if(user) return res.json(obtenerPedidosUsuario(user.idPedidoUser));
    else res.status(404).json(`Index del usuario no encontrado, usuario: ${username} o contraseña: ${password} incorrectos`);
});

router.post('/', (req, res) => {
      

});


module.exports = router;