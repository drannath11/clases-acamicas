
const  pedidos = [
          
    {
          id: 3,
          Pedidos: 0,
          estado: "Pendiente",
          direccionPedido: "Cartagena Bolivar",
          medioDePago: "Efectivo"
          

    }

];

const obtenerPedidos = () => {
    return pedidos;
}

const obtenerPedidosUsuario = (idPedidoUser) => {
    let resultado = []; 
    for (const pedido of pedidos) {
        if(pedido.idPedidosUsuario === idPedidoUser) resultado.push(pedido);
    }
    if(resultado.length > 0) return resultado;
    else  return "El usuario no tiene pedidos";
} 




module.exports = {obtenerPedidos, obtenerPedidosUsuario};