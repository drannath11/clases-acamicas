
const http = require("http");

function servidorWeb(req, res) {
    
    res.writeHead(200, {"content-type": "text/plain"});
    res.end(" Hola Este servidor web esta corriendo");
    
}

let servidor = http.createServer(servidorWeb);

servidor.listen(7070, "192.168.0.200");

console.log("Servidor en le puerto 7070");
